package customer;
import javax.swing.*;

import sql.Jbc;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SignUpFrame extends JFrame implements ActionListener, MouseListener{

    Container container = getContentPane();
    JLabel passwordLabel = new JLabel("Password");
    JPasswordField passwordField = new JPasswordField();
    JButton signUpButton = new JButton("Sign Up");
    JButton resetButton = new JButton("Reset");
    JCheckBox showPassword = new JCheckBox("Show");
    JLabel icon = new JLabel(new ImageIcon(SignUpFrame.class.getResource("logo.png")));
    private JTextField fNameField;
    JFormattedTextField frmtdtxtfldAptno = new JFormattedTextField();
    private JTextField lNameField;
    private JPasswordField passwordField_1;
    private JTextField midNameField;
    private JTextField strtNameField;
    private JTextField txtCity;
    private JTextField txtZip;
    JFormattedTextField streetNoField = new JFormattedTextField();
    JLabel exsUser = new JLabel("Existing User? Log in here");
    private JTextField stateTxt;
    

    SignUpFrame() {
    	getContentPane().setBackground(Color.WHITE);
        setLayoutManager();
        setLocationAndSize();
        addComponentsToContainer();
        addActionEvent();

    }

    public void setLayoutManager() {
        container.setLayout(null); 
    }

    public void setLocationAndSize() {
    	icon.setBounds(257, 13, 250, 250);
        passwordLabel.setBounds(127, 412, 67, 30);
        passwordField.setBounds(206, 416, 131, 22);
        showPassword.setBackground(Color.WHITE);
        showPassword.setBounds(617, 472, 73, 30);
        signUpButton.setBounds(421, 668, 100, 30);
        resetButton.setBounds(257, 668, 100, 30);
        

    }

    public void addComponentsToContainer() {
        container.add(icon);
        container.add(passwordLabel);
        container.add(passwordField);
        container.add(showPassword);
        container.add(signUpButton);
        container.add(resetButton);
        
        JLabel lblFirstName = new JLabel("First Name");
        lblFirstName.setBounds(12, 357, 73, 30);
        getContentPane().add(lblFirstName);
        
        JLabel lblLastName = new JLabel("Last Name");
        lblLastName.setBounds(525, 357, 73, 30);
        getContentPane().add(lblLastName);
        
        fNameField = new JTextField();
        fNameField.setBounds(81, 361, 161, 22);
        getContentPane().add(fNameField);
        fNameField.setColumns(10);
        
        lNameField = new JTextField();
        lNameField.setBounds(595, 361, 161, 22);
        getContentPane().add(lNameField);
        lNameField.setColumns(10);
        
        JLabel lblRepeatPasssword = new JLabel("Repeat Passsword");
        lblRepeatPasssword.setBounds(349, 419, 116, 16);
        getContentPane().add(lblRepeatPasssword);
        
        passwordField_1 = new JPasswordField();
        passwordField_1.setBounds(482, 416, 156, 22);
        getContentPane().add(passwordField_1);
        
        JLabel lblMiddleName = new JLabel("Middle Name");
        lblMiddleName.setBackground(Color.WHITE);
        lblMiddleName.setBounds(257, 364, 87, 16);
        getContentPane().add(lblMiddleName);
        
        midNameField = new JTextField();
        midNameField.setBounds(348, 361, 161, 22);
        getContentPane().add(midNameField);
        midNameField.setColumns(10);
        
        JLabel lblStreetNo = new JLabel("Street No");
        lblStreetNo.setBounds(12, 540, 56, 16);
        getContentPane().add(lblStreetNo);
        
        
        streetNoField.setBounds(81, 537, 56, 22);
        getContentPane().add(streetNoField);
        
        JLabel lblStreetName = new JLabel("Street Name");
        lblStreetName.setBounds(160, 540, 82, 16);
        getContentPane().add(lblStreetName);
        
        strtNameField = new JTextField();
        strtNameField.setBounds(257, 537, 283, 22);
        getContentPane().add(strtNameField);
        strtNameField.setColumns(10);
        
        JLabel lblCity = new JLabel("City");
        lblCity.setBounds(552, 537, 25, 16);
        getContentPane().add(lblCity);
        
        txtCity = new JTextField();
        txtCity.setBounds(595, 537, 161, 22);
        getContentPane().add(txtCity);
        txtCity.setColumns(10);
        
        JLabel lblZip = new JLabel("Zip");
        lblZip.setBounds(358, 595, 25, 16);
        getContentPane().add(lblZip);
        
        txtZip = new JTextField();
        txtZip.setBounds(390, 592, 73, 22);
        getContentPane().add(txtZip);
        txtZip.setColumns(10);
        
        JLabel lblAddress = new JLabel("Address");
        lblAddress.setFont(new Font("Tahoma", Font.BOLD, 17));
        lblAddress.setBounds(358, 488, 73, 36);
        getContentPane().add(lblAddress);
        
        JLabel lblNewLabel = new JLabel("Your Details");
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
        lblNewLabel.setBounds(337, 302, 110, 36);
        getContentPane().add(lblNewLabel);
        
        JLabel lblAptNo = new JLabel("Apt No");
        lblAptNo.setBounds(489, 592, 56, 16);
        getContentPane().add(lblAptNo);
        
        
        frmtdtxtfldAptno.setBounds(545, 589, 61, 22);
        getContentPane().add(frmtdtxtfldAptno);
        
        
        exsUser.setBounds(311, 730, 156, 16);
        getContentPane().add(exsUser);
        
        JLabel lblState = new JLabel("State");
        lblState.setBounds(174, 592, 56, 16);
        getContentPane().add(lblState);
        
        stateTxt = new JTextField();
        stateTxt.setBounds(214, 592, 116, 22);
        getContentPane().add(stateTxt);
        stateTxt.setColumns(10);
    }

    public void addActionEvent() {
    	signUpButton.addActionListener(this);
        resetButton.addActionListener(this);
        showPassword.addActionListener(this);
        exsUser.addMouseListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == signUpButton) {
            //SQL statement
        	if(this.passwordField.getText().contentEquals(this.passwordField_1.getText())) {
        		int addID = -1;
            	
            	String addy = "'" + strtNameField.getText() + "', '" + streetNoField.getText() +"', '"
                        + frmtdtxtfldAptno.getText() + "', '"+ txtCity.getText()+ "', '" + stateTxt.getText()+ "', '"+ txtZip.getText()+"'";
            	
            	ResultSet results;
            	try {
            	Jbc db = new Jbc();
            	db.writeData("insert into Address (StreetName, StreetNo, AptNo, City, State, Zip) VALUES ("+ addy+")");
    			results = db.readData("SELECT * FROM Address where StreetName = '"+strtNameField.getText()+"'");
    			
    			while(results.next()){
    		        addID = results.getInt("AddressID");
    		    }
    			
    			String cust = "'" + this.fNameField.getText() + "', '" + this.midNameField.getText() +"', '"
    		            + this.lNameField.getText() + "', '"+ this.passwordField.getText()+ "', " + addID + ", 0";
    		        	
    			db.writeData("insert into Customer (FirstName, MiddleName, LastName, Password, AddressID, Balance) VALUES ("+ cust+")");
    			
    			this.dispose();
    			} catch (SQLException e1) {
    				// TODO Auto-generated catch block
    				JOptionPane.showMessageDialog(null,"There was an issue, try again.","Sign Up Error",JOptionPane.ERROR_MESSAGE);
    				e1.printStackTrace();
    			}
            	
        	}
        	
        }
        if (e.getSource() == resetButton) {
            passwordField.setText("");
            passwordField_1.setText("");
            fNameField.setText("");
            lNameField.setText("");
            
        }
        if (e.getSource() == showPassword) {
            if (showPassword.isSelected()) {
                passwordField.setEchoChar((char) 0);
                passwordField_1.setEchoChar((char) 0);
            } else {
                passwordField.setEchoChar('*');
                passwordField_1.setEchoChar('*');
            }
       
        }
    }
    
    void openHome(int username) {
    	HomeFrame frame = new HomeFrame(username);
        frame.setTitle("Home");
        frame.setVisible(true);
        frame.setExtendedState(frame.getExtendedState()|JFrame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		 if(arg0.getSource() == exsUser) {
	        	this.dispose();
	        }
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == exsUser) {
			exsUser.setForeground(Color.RED);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == exsUser) {
			exsUser.setForeground(Color.BLACK);
		}
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}



