package customer;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JButton;

public class CartFrame extends JFrame implements ActionListener{
	private JTable cartItems;
	ArrayList<Product> items;
	private String col[] = {"Name","Unit Price","Quantity","Price"};
	private String data[][] = {};
	DefaultTableModel model = new DefaultTableModel(data,col);
	Cart cart;
	int custId;
	JButton btnPlaceOrder = new JButton("Place Order");
	
	public CartFrame(Cart cart, int custId){
		// TODO Auto-generated constructor stub
		this.cart = cart;
		this.custId = custId;
		items = cart.getCart();
		getContentPane().setLayout(null);
				
	}
	void loadScreen() throws SQLException {
		JLabel lblYourCart = new JLabel("Your Cart");
		lblYourCart.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblYourCart.setBounds(245, 13, 89, 16);
		getContentPane().add(lblYourCart);
		getContentPane().setBackground(Color.WHITE);
		
		cartItems = new JTable(loadData());
		cartItems.setBackground(Color.white);
		cartItems.setEnabled(true);
		getContentPane().add(cartItems);
		
		TableRowSorter<TableModel> rowSorter =  new TableRowSorter(cartItems.getModel()); 
		cartItems.setRowSorter(rowSorter);
		
		JTableHeader header = cartItems.getTableHeader();
		header.setBackground(Color.GRAY);
		header.setFont(new Font("Tahoma", Font.BOLD, 13));
		header.setForeground(Color.WHITE);
		
		JScrollPane scrollPane = new JScrollPane(cartItems);
		scrollPane.setBounds(40, 50, 500, 636);
		getContentPane().add(scrollPane);
		
		
		btnPlaceOrder.setBounds(433, 10, 107, 25);
		getContentPane().add(btnPlaceOrder);
		
		btnPlaceOrder.addActionListener(this);
	}
		
	private DefaultTableModel loadData() throws SQLException {
		for(int i = 0; i<this.items.size(); i++){
			double prices = items.get(i).getQuantity() * items.get(i).getPrice();
            Object[] objects = {items.get(i).getName(), items.get(i).getPrice(),items.get(i).getQuantity(), prices};
        
            model.addRow(objects);
        }
        return model;
		}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == btnPlaceOrder) {
			MakeOrder frame = new MakeOrder(items, custId);
	        frame.setTitle("Make your order");
	        frame.setVisible(true);
	        frame.setBounds(10, 10, 600, 600);
	        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	        frame.setResizable(false);
	        
	        dispose();
		}
	}
	
	
}
