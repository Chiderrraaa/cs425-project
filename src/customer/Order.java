package customer;

import java.util.ArrayList;
import java.util.Date;

import staff.Product;

public class Order {
	private Date date_placed;
	private Customer customer;
	private CreditCard card;
	private int quantity;
	private Address address;
	private String status;
	
	public Order(Date date_placed, Customer customer, CreditCard card, int quantity, Address address, String status) {
		this.date_placed = date_placed;
		this.customer = customer;
		this.card = card;
		this.quantity = quantity;
		this.address = address;
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getDate_placed() {
		return date_placed;
	}
	public void setDate_placed(Date date_placed) {
		this.date_placed = date_placed;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public CreditCard getCard() {
		return card;
	}
	public void setCard(CreditCard card) {
		this.card = card;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

}
