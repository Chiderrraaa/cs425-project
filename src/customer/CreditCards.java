package customer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;

import sql.Jbc;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class CreditCards extends JFrame {
	private JTextField txtCcv;
	private JTextField txtExpdate;
	private JTextField txtStname;
	private JTextField txtStno;
	private JTextField txtZip;
	private JTextField txtAptno;
	private JTextField txtCity;
	private JTextField txtState;
	private JTextField txtFname;
	private JTextField txtMname;
	private JTextField txtLname;
	JFormattedTextField txtCardNo = new JFormattedTextField();
	int custId;

	public CreditCards(int custId) {
		// TODO Auto-generated constructor stub
		
		getContentPane().setLayout(null);
		this.custId = custId;
		JLabel lblCardNumber = new JLabel("Card Number");
		lblCardNumber.setBounds(12, 58, 95, 16);
		getContentPane().add(lblCardNumber);
		
		
		txtCardNo.setBounds(101, 55, 184, 22);
		getContentPane().add(txtCardNo);
		
		JLabel lblExpDate = new JLabel("Exp. Date");
		lblExpDate.setBounds(129, 129, 56, 16);
		getContentPane().add(lblExpDate);
		
		JLabel lblCcv = new JLabel("CCV");
		lblCcv.setBounds(469, 126, 56, 16);
		getContentPane().add(lblCcv);
		
		JLabel lblStreetName = new JLabel("Street Name");
		lblStreetName.setBounds(12, 297, 83, 16);
		getContentPane().add(lblStreetName);
		
		JLabel lblStreetNo = new JLabel("Street No");
		lblStreetNo.setBounds(383, 294, 56, 16);
		getContentPane().add(lblStreetNo);
		
		JLabel lblAptNo = new JLabel("Apt No");
		lblAptNo.setBounds(12, 359, 56, 16);
		getContentPane().add(lblAptNo);
		
		JLabel lblCity = new JLabel("City");
		lblCity.setBounds(321, 362, 56, 16);
		getContentPane().add(lblCity);
		
		JLabel lblState = new JLabel("State");
		lblState.setBounds(608, 362, 56, 16);
		getContentPane().add(lblState);
		
		JLabel lblZip = new JLabel("Zip");
		lblZip.setBounds(609, 294, 56, 16);
		getContentPane().add(lblZip);
		
		txtCcv = new JTextField();
		txtCcv.setBounds(523, 123, 116, 22);
		getContentPane().add(txtCcv);
		txtCcv.setColumns(3);
		
		txtExpdate = new JTextField();
		txtExpdate.setBounds(197, 126, 116, 22);
		getContentPane().add(txtExpdate);
		txtExpdate.setColumns(10);
		
		txtStname = new JTextField();
		txtStname.setColumns(10);
		txtStname.setBounds(101, 294, 252, 22);
		getContentPane().add(txtStname);
		
		txtStno = new JTextField();
		txtStno.setBounds(451, 291, 116, 22);
		getContentPane().add(txtStno);
		txtStno.setColumns(10);
		
		txtZip = new JTextField();
		txtZip.setBounds(677, 291, 116, 22);
		getContentPane().add(txtZip);
		txtZip.setColumns(10);
		
		txtAptno = new JTextField();
		txtAptno.setBounds(80, 356, 116, 22);
		getContentPane().add(txtAptno);
		txtAptno.setColumns(10);
		
		txtCity = new JTextField();
		txtCity.setBounds(361, 359, 116, 22);
		getContentPane().add(txtCity);
		txtCity.setColumns(10);
		
		txtState = new JTextField();
		txtState.setBounds(657, 356, 56, 22);
		getContentPane().add(txtState);
		txtState.setColumns(10);
		
		JLabel lblNameOnCard = new JLabel("Name on card:");
		lblNameOnCard.setBounds(297, 58, 95, 16);
		getContentPane().add(lblNameOnCard);
		
		txtFname = new JTextField();
		txtFname.setBounds(404, 55, 116, 22);
		getContentPane().add(txtFname);
		txtFname.setColumns(10);
		
		txtMname = new JTextField();
		txtMname.setBounds(547, 55, 116, 22);
		getContentPane().add(txtMname);
		txtMname.setColumns(10);
		
		txtLname = new JTextField();
		txtLname.setBounds(677, 55, 116, 22);
		getContentPane().add(txtLname);
		txtLname.setColumns(10);
		
		JButton btnAddCard = new JButton("Add Card");
		btnAddCard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Jbc db = new Jbc();
				int addID = -1;
				String delimeter = "-";
				String[] date = txtExpdate.getText().split("/");
            	
            	String addy = "'" + txtStname.getText() + "', '" + txtStno.getText() +"', '"
                        + txtAptno.getText() + "', '"+ txtCity.getText()+ "', '" + txtState.getText()+ "', '"+ txtZip.getText()+"'";
            	
            	ResultSet results;
            	try {
            	db.writeData("insert into Address (StreetName, StreetNo, AptNo, City, State, Zip) VALUES ("+ addy+")");
    			results = db.readData("SELECT * FROM Address where StreetName = '"+txtStname.getText()+"'");
    			
    			while(results.next()){
    		        addID = results.getInt("AddressID");
    		    }
    			
    			String card = txtCardNo.getText() + ", '" + txtFname.getText() +"', '" + txtMname.getText() +"', '"+
    		            txtLname.getText() + "', "+ addID+ ", " + custId + ",'"+date[2]+delimeter+date[1]+delimeter+date[0]+"', '"+txtCcv.getText()+"'";
    		        	
    			db.writeData("insert into Credit_Cards (CardNumber, FirstName, MiddleName, LastName, AddressID, CustomerID, ExpirationDate, cvv) VALUES ("+ card+")");
    			
    			dispose();
    			
    			} catch (SQLException e1) {
    				// TODO Auto-generated catch block
    				JOptionPane.showMessageDialog(null,"There was an issue, try again.","Sign Up Error",JOptionPane.ERROR_MESSAGE);
    				e1.printStackTrace();
    			}
			}
		});
		btnAddCard.setBounds(696, 494, 97, 25);
		getContentPane().add(btnAddCard);
	}
}
