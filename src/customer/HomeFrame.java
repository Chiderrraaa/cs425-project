package customer;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import sql.Jbc;

import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.JToolBar;

public class HomeFrame extends JFrame implements ActionListener {

	int username;
	Container container = getContentPane();
    JLabel searchLabel = new JLabel("Looking for something?");
    JLabel icon = new JLabel(new ImageIcon(HomeFrame.class.getResource("logo.png")));
    JComboBox comboBox = new JComboBox();
    ArrayList<Product> products = new ArrayList<>();
    JButton btnMyCreditCards = new JButton("My Credit Cards");
    public Cart cart = new Cart();
    private final JButton btnGoToCart = new JButton("Go to Cart");
    JButton btnMyAddress = new JButton("My Address");
    HomeFrame(int username) {
    	getContentPane().setBackground(Color.WHITE);
    	this.username = username;
    	
        setLayoutManager();
        setLocationAndSize();
        addComponentsToContainer();
        addActionEvent();

    }

    public void setLayoutManager() {
        container.setLayout(null); 
    }

    public void setLocationAndSize() {
        searchLabel.setBounds(317, 425, 139, 30);
        icon.setBounds(255, 132, 242, 252);
    }

    public void addComponentsToContainer() {
    	container.add(icon);
        container.add(searchLabel);
        comboBox.addPopupMenuListener(new PopupMenuListener() {
        	public void popupMenuCanceled(PopupMenuEvent arg0) {
        	}
        	public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) {
        		viewProduct(products.get(comboBox.getSelectedIndex()), cart);
        	}
        	public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) {
        	}
        });
                
       
        comboBox.setEditable(true);
        comboBox.setBounds(97, 486, 587, 30);
        
        fillComboBox();
        AutoCompletion.enable(comboBox);
        getContentPane().add(comboBox);
        btnGoToCart.setBounds(674, 13, 97, 25);
        
        getContentPane().add(btnGoToCart);
        
       
        btnMyCreditCards.setBounds(12, 13, 130, 25);
        getContentPane().add(btnMyCreditCards);
        
       
        btnMyAddress.setBounds(163, 13, 123, 25);
        getContentPane().add(btnMyAddress);
        
        
    }

    public void addActionEvent() {
    	btnGoToCart.addActionListener(this);
    	btnMyCreditCards.addActionListener(this);
    	btnMyAddress.addActionListener(this);
    }
    
    void fillComboBox() {
    	ResultSet rs;
		
		Jbc db = new Jbc();
		try {
			rs = db.readData("Select * FROM Products");
	        
			while(rs.next()){
		        	comboBox.addItem(rs.getString("Name"));   
		        	Product p = new Product(rs.getString("Name"), rs.getString("Brand"), rs.getString("Type"), rs.getInt("Size"), rs.getDouble("Price"));
		        	p.setP_id(rs.getInt("Product_ID"));
		        	p.setAlcohol(rs.getString("AlcoholContent"));
		        	p.setNutrition(rs.getString("Nutrition"));
		        	
		        	products.add(p);
			}
		     		       
		} catch (Exception e1) {
			e1.printStackTrace();
		}
    }

 
    
    void viewProduct(Product p, Cart cart) {
    	ProductFrame frame = new ProductFrame(p, cart);
        frame.setTitle(p.getName());
        frame.setVisible(true);
        frame.setBounds(10, 10, 620, 450);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setResizable(false);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == btnGoToCart) {
			CartFrame frame = new CartFrame(cart, username);
	        frame.setTitle("Your Cart");
	        try {
				frame.loadScreen();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        frame.setVisible(true);
	        frame.setBounds(10, 10, 600, 750);
	        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	        frame.setResizable(false);
		}else if(e.getSource() == btnMyCreditCards) {
			ViewCards frame = new ViewCards(username);
	        frame.setTitle("My Credit Cards");
	        frame.setVisible(true);
	        frame.setBounds(10, 10, 575, 650);
	        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	        frame.setResizable(false);
		}
	}
}
