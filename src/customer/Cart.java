package customer;

import java.util.ArrayList;

public class Cart {
	private static ArrayList<Product> items = new ArrayList<Product>();
	public Cart() {
		// TODO Auto-generated constructor stub
	}
	public void addItem(Product item) {
		items.add(item);
	}
	
	public ArrayList<Product> getCart() {
		return items;
	}
	
	public void deleteItem(int index) {
		items.remove(index);
	}

}
