package customer;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.JButton;

public class ProductFrame extends JFrame implements ActionListener{

	Container container = getContentPane();
	Product product;
	JButton btnAddToCart = new JButton("Add to Cart");
	JSpinner spinner = new JSpinner();
	Cart cart;
	public ProductFrame(Product p, Cart c) {
		getContentPane().setBackground(Color.WHITE);
		// TODO Auto-generated constructor stub
		this.product = p;
		this.cart= c;
		setLayoutManager();
	}
	
	 public void setLayoutManager() {
	        container.setLayout(null);
	        
	        JLabel lblProductName = new JLabel(product.getName());
	        lblProductName.setFont(new Font("Tahoma", Font.BOLD, 23));
	        lblProductName.setBounds(47, 34, 477, 29);
	        getContentPane().add(lblProductName);
	        
	        JLabel lblProductId = new JLabel("PRODUCT ID: "+product.getP_id());
	        lblProductId.setFont(new Font("Arial Black", Font.BOLD, 13));
	        lblProductId.setBounds(47, 88, 316, 16);
	        getContentPane().add(lblProductId);
	        
	        JLabel lblBrand = new JLabel("Brand: "+ product.getBrand());
	        lblBrand.setFont(new Font("Arial Black", Font.BOLD, 13));
	        lblBrand.setBounds(47, 111, 316, 16);
	        getContentPane().add(lblBrand);
	        
	        JLabel lblSize = new JLabel("Size: "+ product.getSize());
	        lblSize.setFont(new Font("Arial Black", Font.BOLD, 13));
	        lblSize.setBounds(47, 198, 260, 16);
	        getContentPane().add(lblSize);
	        	        
	        spinner.setValue(1);
	        spinner.setBounds(517, 231, 50, 22);
	        getContentPane().add(spinner);
	        
	        JLabel lblQuantity = new JLabel("Quantity");
	        lblQuantity.setFont(new Font("Arial Black", Font.BOLD, 13));
	        lblQuantity.setBounds(440, 233, 79, 16);
	        getContentPane().add(lblQuantity);
	        
	        btnAddToCart.setBounds(470, 286, 97, 25);
	        getContentPane().add(btnAddToCart);
	        btnAddToCart.addActionListener(this);
	        
	        JLabel lblType = new JLabel("Type: "+ product.getType());
	        lblType.setFont(new Font("Arial Black", Font.BOLD, 13));
	        lblType.setBounds(47, 140, 316, 16);
	        getContentPane().add(lblType);
	        
	        JLabel lblNutrition = new JLabel("Nutrition: "+ product.getNutrition());
	        lblNutrition.setFont(new Font("Arial Black", Font.BOLD, 13));
	        lblNutrition.setBounds(47, 169, 316, 16);
	        getContentPane().add(lblNutrition);
	        
	        JLabel lblAlcoholic = new JLabel("Alcoholic Content: "+ product.getAlcohol());
	        lblAlcoholic.setFont(new Font("Arial Black", Font.BOLD, 13));
	        lblAlcoholic.setBounds(47, 227, 260, 16);
	        getContentPane().add(lblAlcoholic);
	        
	        JLabel lblPrice = new JLabel("$"+ product.getPrice());
	        lblPrice.setForeground(Color.CYAN);
	        lblPrice.setFont(new Font("Arial Black", Font.BOLD, 20));
	        lblPrice.setBounds(437, 34, 130, 37);
	        getContentPane().add(lblPrice);
	    }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource() == btnAddToCart) {
			product.setQuantity((Integer)spinner.getValue());
			cart.addItem(this.product);
			this.dispose();
		}
	}
	
	
}
