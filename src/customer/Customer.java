package customer;

public class Customer {
	private int id; 
	private String name;
	private int balance;
	private CreditCard card;
	private Address add;
	
	public Customer(int id, String name, CreditCard card, Address add) {
		this.id = id;
		this.name = name;
		this.card = card;
		this.add = add;
		this.balance = 0;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public CreditCard getCard() {
		return card;
	}

	public void setCard(CreditCard card) {
		this.card = card;
	}

	public Address getAdd() {
		return add;
	}

	public void setAdd(Address add) {
		this.add = add;
	}

}
