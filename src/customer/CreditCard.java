package customer;

import java.util.Date;

public class CreditCard {
	private int cardNo;
	private String cardName;
	private String expDate;
	private String cvv;
	
	public CreditCard(int cardNo, String cardName, String expDate, String cvv) {
		this.cardNo = cardNo;
		this.cardName = cardName;
		this.expDate = expDate;
		this.cvv = cvv; 
	}

	public int getCardNo() {
		return cardNo;
	}

	public void setCardNo(int cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

}
