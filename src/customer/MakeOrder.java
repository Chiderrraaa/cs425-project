package customer;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.util.Date;
import sql.Jbc;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class MakeOrder extends JFrame{
ArrayList<Product> products;
int custId;
JComboBox comboBox = new JComboBox();
JComboBox comboBox_1 = new JComboBox();
	public MakeOrder(ArrayList<Product> products, int custId) {
		// TODO Auto-generated constructor stub
		this.custId = custId;
		this.products = products;
		
		getContentPane().setLayout(null);
		fillComboBox();
		
		comboBox.setBounds(26, 106, 460, 22);
		getContentPane().add(comboBox);
		
		
		comboBox_1.setBounds(26, 276, 460, 22);
		getContentPane().add(comboBox_1);
		
		JLabel lblChooseShippingAddress = new JLabel("Choose Shipping Address");
		lblChooseShippingAddress.setBounds(12, 61, 185, 16);
		getContentPane().add(lblChooseShippingAddress);
		
		JLabel lblChooseYourCredit = new JLabel("Choose your credit card");
		lblChooseYourCredit.setBounds(12, 235, 158, 16);
		getContentPane().add(lblChooseYourCredit);
		
		JButton btnPlaceOrder = new JButton("Place order");
		btnPlaceOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date today = new Date();
				
				int addId = 0;
				ResultSet ts;
				for(Product product: products) {
					Jbc db = new Jbc();
					try {
						ts = db.readData("Select * FROM Address where StreetName = '"+comboBox.getSelectedItem().toString()+"'");
						while(ts.next()){
				        addId = ts.getInt("AddressID");
						}
						String statement = custId + ", " + Integer.parseInt(comboBox_1.getSelectedItem().toString())+", "+ product.getP_id()+", "+addId+", "+product.getQuantity()+", '"+dateFormat.format(today)+"', 'issued'";
						db.writeData("insert into Orders (CustomerID, CardNumber, Product_ID, AddressID, Quantity, Date_placed, Status) VALUES ("+ statement+")");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				     
					JOptionPane.showMessageDialog(null,"Your order has been placed","Successful",JOptionPane.INFORMATION_MESSAGE);
					dispose();
				}
				
			}
		});
		btnPlaceOrder.setBounds(386, 385, 112, 25);
		getContentPane().add(btnPlaceOrder);
	}
	
	 void fillComboBox() {
	    	ResultSet rs, ts;
			
			Jbc db = new Jbc();
			try {
				rs = db.readData("Select * FROM Address INNER JOIN Customer ON Address.AddressID = Customer.AddressID where Customer.CustomerID ="+ custId);
		        ts = db.readData("Select * FROM credit_cards where CustomerID = "+custId);
				while(rs.next()){
					ts.next();
			        	comboBox.addItem(rs.getString("StreetName"));   
			        	comboBox_1.addItem(ts.getInt("CardNumber"));
				}
			     		       
			} catch (Exception e1) {
				e1.printStackTrace();
			}
	    }

}
